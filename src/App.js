import './App.css';
import Menu from './Components/Menu.js';
import Reviews from './Components/Reviews.js';
import Attractions from './Components/Attractions.js';
import Cities from './Components/Cities.js';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import NavBar from './Components/NavBar';
import About from './Components/About.js';
import React, { useState } from 'react';

class App extends React.Component {

  state = {
    city: "Austin",
    attraction: "9nUrDQ_REhU6sgcgXFAyfA",
  };


  render() {
    return (
        <div class="background">
          <BrowserRouter>
            <NavBar></NavBar>
            <Routes>
              <Route path="/" element={<Menu />} />
              <Route path="/home" element={<Menu />} />
              <Route path="/Reviews" element={<Reviews />} />
              <Route path="/Reviews/:ID" element={<Reviews />} />
              <Route path="/Attractions/:city" element={<Attractions />} />
              <Route path="/Attractions" element={<Attractions />} />
              <Route path="/About" element={<About />} />
              <Route path="/Cities" element={<Cities />} />
            </Routes>
          </BrowserRouter>
        </div>
    );
  }
}

export default App;
