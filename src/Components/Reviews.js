import React, { useState, useEffect } from 'react';
import { Col, Container, Row, Card } from 'react-bootstrap';
import axios from 'axios';
import Pagination from './Pagination';
import { useParams } from 'react-router-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

const Reviews = () => {
  const [reviews, setReviews] = useState([]);
  const [error, setError] = useState(null);
  const [searchQuery, setSearchQuery] = useState('');
  const [sortOption, setSortOption] = useState('');
  const { ID } = useParams();
  const [reviewID, setReviewID] = useState(ID || 'bQaiow1WcwwSODUo9hHOMQ');
  const [currentPage, setCurrentPage] = useState(1);
  const reviewsPerPage = 6;

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          `https://testdatabase-392302.uc.r.appspot.com/reviews?search=${searchQuery}&sort=${sortOption}&reviewID=${reviewID}`
        );
        setReviews(response.data);
      } catch (error) {
        setError(error);
      }
    };

    fetchData();
  }, [searchQuery, sortOption, reviewID]);

  const changeAPI = async () => {
    try {
      const response = await axios.get(
        `https://testdatabase-392302.uc.r.appspot.com/reviews?search=${searchQuery}&sort=${sortOption}&reviewID=${reviewID}`
      );
      setReviews(response.data);
    } catch (error) {
      setError(error);
    }
  };

  const handleSearchQueryChange = (event) => {
    const searchQuery = event.target.value;
    setSearchQuery(searchQuery);
  };

  const handleSortOptionChange = (event) => {
    const sortOption = event.target.value;
    setSortOption(sortOption);
  };

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const indexOfLastReview = currentPage * reviewsPerPage;
  const indexOfFirstReview = indexOfLastReview - reviewsPerPage;
  const currentReviews = reviews.slice(indexOfFirstReview, indexOfLastReview);

  if (error) {
    return (
      <div>
        <h1>Error</h1>
        <p>{error.message}</p>
      </div>
    );
  } else {
    return (
      <div className="backgroundReviews">
        <br />
        <div
          style={{
            border: '10px solid white',
            padding: '1px',
            backgroundColor: 'white',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            height: '100%',
          }}
        >
          <p style={{ fontSize: 50, textAlign: 'center', fontWeight: '800', margin: 'auto' }}>
            Authentic Travel Experiences: User Reviews and Ratings
          </p>
        </div>
        <br />
        <Container>
          <Row>
            <Col>
              <div className="search-container">
                <input
                  type="text"
                  placeholder="Search by Name"
                  value={searchQuery}
                  onChange={handleSearchQueryChange}
                />
                <select onChange={handleSortOptionChange} className="sort-dropdownReviews">
                  <option value="">Sort by</option>
                  <option value="Rating">Rating</option>
                </select>
              </div>
            </Col>
          </Row>
          <Row>
            {currentReviews.map((review, i) => (
              <Col key={i} xs={12} sm={6} md={4}>
                <Card style={{ width: '100%', margin: '20px' }} border={'success'} bg={'light'} text={'dark'}>
                  <Card.Body>
                    <Card.Title>{review.name}</Card.Title>
                    <Card.Img src={review.image_url} style={{ width: '200px' }} />
                    <Card.Text>
                      <ul>
                        <li>
                          <a href={review.Link}>Link to Review!</a>
                        </li>
                        <li>{review.rText}</li>
                        <li>Rating: {review.Rating}</li>
                      </ul>
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            ))}
          </Row>
          <Pagination
            nPages={Math.ceil(reviews.length / reviewsPerPage)}
            currentPage={currentPage}
            setCurrentPage={handlePageChange}
          />
        </Container>
      </div>
    );
  }
};

export default Reviews;
